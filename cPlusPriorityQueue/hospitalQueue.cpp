    /*
     * C++ Program to Implement Priority Queue
     * In a hospital patient queueing
     */
    #include <iostream>
    #include <cstdio>
    #include <cstring>
    #include <cstdlib>
    using namespace std;
    /*
     * Node Declaration
     */
    struct node
    {
    	int priority;
    	char* name;
    	struct node *link;
    };
    /*
     * Class Priority Queue
     */
    class Priority_Queue
    {
        private:
            node *front;
        public:
            Priority_Queue()
            {
                front = NULL;
            }
             //* Insert into Priority Queue
            void insert(char* name , int priority)
            {
                node *tmp, *q;
		char* pname=new char[30];
                tmp = new node;
                strcpy(pname,name);
                tmp->priority = priority;
                tmp->name = pname;
                if (front == NULL || priority > front->priority)
                {
                    tmp->link = front;
                    front = tmp;
                }
                else
                {
                    q = front;
                    while (q->link != NULL && q->link->priority <= priority)
                        q=q->link;
                    tmp->link = q->link;
                    q->link = tmp;
                }
            }
            
            // * Delete from Priority Queue
            void del()
            {
                node *tmp;
                if(front == NULL)
                    cout<<"Queue Underflow\n";
                else
                {
                    tmp = front;
                    cout<<"\nAdmitted patient is: "<<tmp->name<<endl<<endl;
                    front = front->link;
                    free(tmp);
                }
            }
            /*
             * Print Priority Queue
             */
            void display()
            {
		int c=0;
                node *ptr;
                ptr = front;
                if (front == NULL)
                    cout<<"Queue is empty\n";
                else
                {	cout<<"\nQueue is :\n";
                    cout<<"\n\nNum\tPrior\tPatient\n";
                    while(ptr != NULL)
                    {
                        cout<<++c<<"\t"<<ptr->priority<<"\t"<<ptr->name<<endl;
                        ptr = ptr->link;
                    }
                }
		cout<<"\n\n";
            }
    };
    /*
     * Main
     */
    int main()
    {
        int choice, priority;
	char* name;
        Priority_Queue pq; 
        do
        {
            cout<<"\n1.Add Patient\n";
            cout<<"2.Admit\n";
            cout<<"3.Display\n";
            cout<<"4.Quit\n";
            cout<<"Enter your choice : "; 
            cin>>choice;
            switch(choice)
            {
            case 1:
                cout<<"\nInput the patient name : ";
                cin>>name;
                cout<<"Enter his/her condition\n1LOW-Flu,Infection,Fever,Cold, etc\n2MEDIUM-Dengue,Malaria, etc\n3HIGH-Fracture,Uncousncess,Stroke, etc.\nchoose : ";
                cin>>priority;
                pq.insert(name, priority);
                break;
            case 2:
                pq.del();
                break;
            case 3:
                pq.display();
                break;
            case 4:
                break;
            default :
                cout<<"Wrong choice\n";
            }
        }
        while(choice != 4);
        return 0;
    }
